minetest.register_chatcommand("list", {
	description = "List players on the server",
	params = "",
	privs = {},
	func = function(name, param)
		local playerstring = ""
		for i, player in ipairs(minetest.get_connected_players()) do
			local name = player:get_player_name()
			local lname = ""
			if minetest.get_modpath("rank") then
				if vanished_players[name] then
					lname = "[" .. minetest.colorize(rank_colors[ranks[name]], ranks[name]) .. "] " .. minetest.colorize("#ffffff7f", name)
				else
					lname = "[" .. minetest.colorize(rank_colors[ranks[name]], ranks[name]) .. "] " .. name
				end
			else
				if vanished_players[name] then
					lname = minetest.colorize("#ffffff7f", name)
				else
					lname = name
				end
			end
			if i < #minetest.get_connected_players() then
				playerstring = playerstring..lname..", "
			else
				playerstring = playerstring..lname
			end
		end
		if math.random(1, 5) == 1 and minetest.settings:get_bool("enable_particles") then
			easter_egg(name)
		end
		minetest.chat_send_player(name, playerstring)
	end
})

minetest.log('action',"["..lunocoms.modname:upper().."] Command 'list' loaded!")