# [LUNOCOMS]

Alguns comandos adicionais para o minetest.

<!--
![screenshot]
-->

**Download:**
<!-- 
* [0.0.1] → Click on link, or do the terminal comand: `git clone -b "1.1.1" --single-branch https://gitlab.com/lunovox/lunocoms.git `
-->

 * [Repository] → Download only per the terminal comand: `git clone https://gitlab.com/lunovox/lunocoms.git `

**Dependencies:**
  * [default] → Minetest Game Included (version 0.4.17.1 or more)
<!--
**Optional Dependencies:**
  * [intllib] → Facilitates the translation of several other mods into your native language, or other languages.
  * [tradelans] → Protect your land through biweekly payment using money earned in the game.
  -->
**Licence:**
 * GNU AGPL: https://github.com/Lunovox/outdoor/blob/master/LICENSE

**Developers:**
 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http://qoto.org/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [mumble](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)
 
[1.1.1]:https://gitlab.com/lunovox/outdoor/-/archive/1.1.1/outdoor-1.1.1.zip
[default]:https://content.minetest.net/packages/Minetest/minetest_game/
[intllib]:https://github.com/minetest-mods/intllib
[OUTDOOR]:https://gitlab.com/lunovox/outdoor
[Repository]:https://gitlab.com/lunovox/outdoor
[screenshot]:https://gitlab.com/lunovox/outdoor/-/raw/master/screenshot.png
[tradelans]:https://github.com/Lunovox/tradelands
