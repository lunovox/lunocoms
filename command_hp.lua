minetest.register_privilege("hp", {description = "Can change the hp of a player", give_to_singleplayer = false})

minetest.register_chatcommand("hp", {
	description = "Set <player>'s hp",
	params = "<player> [hp]",
	privs = {hp = true},
	func = function(name, param)
		local params = param:split(' ')
		if not params[1] then params[1] = "" end
		local target = minetest.get_player_by_name(params[1])
		local hp = tonumber(params[2])
		
		if target and hp then
			target:set_hp(hp)
			minetest.chat_send_player(name, "Set "..target:get_player_name().."'s hp to "..hp..".")
			minetest.chat_send_player(target:get_player_name(), minetest.colorize("#FFFF00", name.." changed your hp to "..hp.."!"))
			if minetest.settings:get_bool("enable_command_feedback") then
				minetest.chat_send_all(minetest.colorize("#7F7F7F", "["..name..": Set "..target:get_player_name().."'s hp.]"))
			end
			if math.random(1, 5) == 1 and minetest.settings:get_bool("enable_particles") then
				easter_egg(target:get_player_name())
			end
		elseif target and not hp then
			minetest.chat_send_player(name, params[1].."'s hp is "..tostring(target:get_hp())..".")
			if minetest.settings:get_bool("enable_command_feedback") then
				minetest.chat_send_all(minetest.colorize("#7F7F7F", "["..name..": Queried "..target:get_player_name().."'s hp.]"))
			end
			if math.random(1, 5) == 1 and minetest.settings:get_bool("enable_particles") then
				easter_egg(target:get_player_name())
			end
		else
			minetest.chat_send_player(name, minetest.colorize("#FF0000", "Invalid usage."))
		end
	end
})

minetest.log('action',"["..lunocoms.modname:upper().."] Command 'hp' loaded!")


