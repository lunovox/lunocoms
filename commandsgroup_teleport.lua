lunocoms.back_pos = {}

function lunocoms.easter_egg(name)
	local pos = minetest.get_player_by_name(name):getpos()
	minetest.add_particlespawner({
		amount = 20,
		time = 0.1,
		minpos = pos,
		maxpos = {x = pos.x, y = pos.y + 2, z = pos.z},
		minvel = {x = -1, y = -1, z = -1},
		maxvel = {x = 1, y = -0, z = 1},
		minacc = {x = -1, y = -1, z = -1},
		maxacc = {x = 1, y = 1, z = 1},
		minsize = 1,
		maxsize = 1,
		minexptime = 0.5,
		maxexptime = 0.75,
		collisiondetection = true,
		vertical = false,
		texture = "particle_0.png^[colorize:#"..rand_color()
	})
	minetest.add_particlespawner({
		amount = 20,
		time = 0.1,
		minpos = pos,
		maxpos = {x = pos.x, y = pos.y + 2, z = pos.z},
		minvel = {x = -1, y = -1, z = -1},
		maxvel = {x = 1, y = -0, z = 1},
		minacc = {x = -1, y = -1, z = -1},
		maxacc = {x = 1, y = 1, z = 1},
		minexptime = 0.5,
		maxexptime = 0.75,
		minsize = 1,
		maxsize = 1,
		collisiondetection = true,
		vertical = false,
		texture = "particle_1.png^[colorize:#"..rand_color()
	})
	minetest.add_particlespawner({
		amount = 20,
		time = 0.1,
		minpos = pos,
		maxpos = {x = pos.x, y = pos.y + 2, z = pos.z},
		minvel = {x = -1, y = -1, z = -1},
		maxvel = {x = 1, y = -0, z = 1},
		minacc = {x = -1, y = 1, z = -1},
		maxacc = {x = 1, y = -1, z = 1},
		minexptime = 0.5,
		maxexptime = 0.75,
		minsize = 1,
		maxsize = 1,
		collisiondetection = true,
		vertical = false,
		texture = "particle_2.png^[colorize:#"..rand_color()
	})
end

--#####################################################################################################
-- Add /back support for returning to death point.

minetest.register_on_dieplayer(function(player)
        local pos = player:getpos()
        lunocoms.back_pos[player:get_player_name()] = vector.round(pos)
        minetest.chat_send_player(player:get_player_name(), "Type /back to return to your death point.")
end)

--#####################################################################################################

minetest.register_chatcommand("thru", {
	privs = {teleport = true},
	description = "Teleport to a free space in front of you.",
	params = "",
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local dir = minetest.facedir_to_dir(minetest.dir_to_facedir(player:get_look_dir()))
		local pos = vector.round(player:getpos())
		local i, j
		local found = false

		lunocoms.back_pos[name] = pos

		for i = 1, 10 do
			if not soft(get_front(pos, dir, i)) then
				found = true
				j = i
				break
			end
		end

		if not found then
			minetest.chat_send_player(name, minetest.colorize("#ff0000", "Wall not found."))
			return
		end

		for i = 1, 10 do
			if soft(get_front(pos, dir, i)) and i > j then
				player:setpos(get_front(pos, dir, i))
				minetest.chat_send_player(name, "Node found!")
				if math.random(1, 5) == 1 and minetest.settings:get_bool("enable_particles") then
					lunocoms.easter_egg(name)
				end
				return
			end
		end
		minetest.chat_send_player(name, minetest.colorize("#ff0000", "No soft nodes found."))
	end
})

--#####################################################################################################

minetest.register_chatcommand("back", {
        description = "Return to previous position",
        privs = {back = true},
        params = "",
        func = function(name, param)
                local player = minetest.get_player_by_name(name)
                if lunocoms.back_pos[name] then
                        player:setpos(lunocoms.back_pos[name])
                        if math.random(1, 5) == 1 and minetest.settings:get_bool("enable_particles") then
                                lunocoms.easter_egg(name)
                        end
                        minetest.chat_send_player(name, "You have been returned to " .. minetest.pos_to_string(lunocoms.back_pos[name]) .. ".")
                        lunocoms.back_pos[name] = nil
                else
                        minetest.chat_send_player(name, minetest.colorize("#ff0000", "Nowhere to return to."))
                end
        end
})

--#####################################################################################################

local act = minetest.chatcommands["teleport"].func
minetest.override_chatcommand("teleport", {
	func = function(name, param)
		local pos = minetest.get_player_by_name(name):getpos()
		lunocoms.back_pos[name] = vector.round(pos)
		local text, good

		good, text = act(name, param)
		if text then
			minetest.chat_send_player(name, text)
			if good then
				lunocoms.back_pos[name] = vector.round(pos)
			end
		end
	end
})
minetest.register_chatcommand("tp", minetest.chatcommands["teleport"])

--#####################################################################################################
-- Add /back support for the /killme command.

local comKillMe = minetest.chatcommands["killme"]
if comKillMe then
	local act = comKillMe.func
	minetest.override_chatcommand("killme", {
		func = function(name, param)
			local pos = minetest.get_player_by_name(name):getpos()
			local text, good
			
			good, text = act(name, param)
			if text then
				minetest.chat_send_player(name, text)
				if good then
					lunocoms.back_pos[name] = vector.round(pos)
				end
			end
		end
	})
end

--#####################################################################################################
-- Add /back support for the /home command.

local act = minetest.chatcommands["home"].func
minetest.override_chatcommand("home", {
	func = function(name, param)
		local pos = minetest.get_player_by_name(name):getpos()
		lunocoms.back_pos[name] = vector.round(pos)
		local text, good
		good, text = act(name, param)
		if text then
			minetest.chat_send_player(name, text)
			if good then
				lunocoms.back_pos[name] = vector.round(pos)
			end
		end
	end
})

minetest.log('action',"["..lunocoms.modname:upper().."] Commands of 'teleport' loaded!")